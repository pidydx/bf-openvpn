#########################
# Create base container #
#########################
FROM ubuntu:24.04 as base
LABEL maintainer="pidydx"

# Base setup
ENV BASE_PKGS iptables openvpn openvpn-auth-ldap 

# Update users and groups
RUN userdel ubuntu

# Update and install base packages
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ca-certificates \
 && apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_PKGS} \
 && rm -rf /var/lib/apt/lists/*


##########################
# Create final container #
##########################
FROM base

# Prepare container
COPY etc/ /etc/
COPY usr/ /usr/

EXPOSE 443/tcp 1194/udp
VOLUME ["/etc/openvpn"]

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["openvpn"]
