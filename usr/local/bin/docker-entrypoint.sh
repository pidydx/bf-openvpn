#!/bin/bash

set -e

if [ "$1" = 'openvpn' ]; then
    mkdir -p /dev/net
    mknod /dev/net/tun c 10 200 
    mkdir -p /var/run/openvpn
    openssl dhparam -out /var/run/openvpn/dh.pem 2048
    iptables-restore /etc/openvpn/iptables.rules
    exec openvpn --config /etc/openvpn/server.conf
fi

exec "$@"
